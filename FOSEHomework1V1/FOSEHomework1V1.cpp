﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

void CreateRandomMatrix(int**& matrix, const int SIZE);
void PrintMatrix(int**& matrix, const int SIZE);
void DeleteMartix(int**& matrix, const int SIZE);
void Task1(int**& matrix, const int SIZE);
void Task2(int**& matrix, const int SIZE);
void Task3(int**& matrix, const int SIZE);

int main()
{
    srand(time(nullptr));
    setlocale(LC_ALL, "ru");
    int size{};
    cout << "Количество строк и столбцов: ";
    cin >> size;
    int** matrix = nullptr;
    CreateRandomMatrix(matrix, size);
    PrintMatrix(matrix, size);
    Task1(matrix, size);
    Task2(matrix, size);
    Task3(matrix, size);
    DeleteMartix(matrix, size);
    return 0;
}

void CreateRandomMatrix(int**& matrix, const int SIZE)
{
    const int MIN_ELEMENT = -10;
    const int MAX_ELEMENT = 10;
    matrix = new int* [SIZE];
    for (int i = 0; i < SIZE; i++)
    {
        matrix[i] = new int[SIZE];
        for (int j = 0; j < SIZE; j++)
        {
            matrix[i][j] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
        }
    }
}

void PrintMatrix(int**& matrix, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            cout << setw(4) << matrix[i][j];
        }
        cout << "\n";
    }
}

void DeleteMartix(int**& matrix, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
    {
        delete[] matrix[i];
    }
}

void Task1(int**& matrix, const int SIZE)
{
    cout << "\nПоиск наибольших элементов в четных столбцах\n";
    for (int j = 1; j < SIZE; j += 2)
    {
        int maxElement = INT_MIN;
        for (int i = 0; i < SIZE; i++)
        {
            if (matrix[i][j] > maxElement)
            {
                maxElement = matrix[i][j];
            }
        }
        cout << j + 1 << " столбец: " << maxElement << endl;
    }
}

void Task2(int**& matrix, const int SIZE)
{
    for (int i = 0; i < SIZE; i++)
    {
        if (matrix[i][i] != matrix[i][SIZE - 1 - i])
        {
            cout << "Главная и побочная диагонали не совпадают.\n";
            return;
        }
    }
    cout << "Главная и побочная диагонали совпадают.\n";
}

void Task3(int**& matrix, const int SIZE)
{
    int minElement = INT_MAX;
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            if (matrix[i][j] < minElement)
            {
                minElement = matrix[i][j];
            }
        }
    }
    cout << "Минимальное элемент в выделенной части матрицы " << minElement << ".\n";
}